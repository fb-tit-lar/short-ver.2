<?php $__env->startSection('content'); ?>
    <div class="container">
        <h1 class="mt-3" style="text-align: center">LIST SHORTENER URL
            <form method="real" action="<?php echo e(url('/new')); ?>">
                <?php echo csrf_field(); ?>
                <button type="submit" class="btn btn-outline-secondary" style="margin-top: 20px;">CREATE SHORT URL</button>
            </form>
        </h1>
        <table class="table table-striped table-dark">
            <thead>
            <tr>
                <th scope="col">TIME</th>
                <th scope="col">LONG</th>
                <th scope="col">SHORT</th>
                <th scope="col">COPY</th>
                <th scope="col">VIEW</th>
            </tr>
            </thead>
            <tbody>
            <?php if(count($shortens)>0): ?>
                <?php $__currentLoopData = $shortens; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $shorten): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                    <tr>
                        <td>
                            <p><?php echo e($shorten->created_at); ?></p>
                        </td>

                        <td>
                            <a href="<?php echo e(url($shorten->long)); ?>" >
                                <p class="text-warning"><?php echo e($shorten->long); ?></p>
                            </a>
                        </td>
                        <td>
                            <input id="shorturl<?php echo e($shorten->id); ?>" class="form-control" type="text" value="http://www.short.local/t/<?php echo e($shorten->short); ?>" readonly>
                        </td>

                        <td>
                            <button onclick="copy(this)" id="copyBtn" value="<?php echo e($shorten->id); ?>" type="button" class="btn btn-info">copy</button>
                        </td>

                        <td>
                            <p><?php echo e($shorten->view); ?></p>
                        </td>
                    </tr>
                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
            <?php endif; ?>

            </tbody>
        </table>
    </div>






<?php $__env->stopSection(); ?>

<script>
    function copy(clickedBtn) { //clickedBtn คือ paramiter เฉยๆ
        var id = clickedBtn.value;
        var copyText = document.querySelector('#shorturl'+id);
        copyText.select();
        document.execCommand('copy');//function copy ของ javascript
        alert('Copied '+ copyText.value);
    }
</script>

<?php echo $__env->make('layouts.app', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /Applications/XAMPP/xamppfiles/htdocs/short/resources/views/index.blade.php ENDPATH**/ ?>