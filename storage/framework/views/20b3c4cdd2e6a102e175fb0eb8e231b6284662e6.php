<?php $__env->startSection('content'); ?>
    <h1 class="mt-3" style="text-align: center">SHORTEN URL</h1>

    <form method="new" action="<?php echo e(url('/')); ?>" style="text-align: center">
        <?php echo csrf_field(); ?>
        <button type="submit" class="btn btn-outline-secondary" style="margin-top: 15px;">VIEW SHORT URL</button>
    </form>


    <div class="card text-center" style="margin-top: 20px;">
        <div class="card-header">
            <label>Long URL</label>

        <form method="post" action="<?php echo e(url('/')); ?>">
            <?php echo csrf_field(); ?>
            <div class="form-group">
                <input type="text" name="long" class="form-control">
            </div>

            <button type="submit" class="btn btn-primary btn-info" style="margin-top: 5px;">CREATE SHORT URL</button>
        </form>

        </div>
    </div>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.app', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /Applications/XAMPP/xamppfiles/htdocs/short/resources/views/new.blade.php ENDPATH**/ ?>