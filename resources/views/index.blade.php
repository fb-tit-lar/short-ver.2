@extends('layouts.app')
@section('content')
    <div class="container">
        <h1 class="mt-3" style="text-align: center">LIST SHORTENER URL
            <form method="real" action="{{url('/new')}}">
                @csrf
                <button type="submit" class="btn btn-outline-secondary" style="margin-top: 20px;">CREATE SHORT URL</button>
            </form>
        </h1>
        <table class="table table-striped table-dark">
            <thead>
            <tr>
                <th scope="col">TIME</th>
                <th scope="col">LONG</th>
                <th scope="col">SHORT</th>
                <th scope="col">COPY</th>
                <th scope="col">VIEW</th>
            </tr>
            </thead>
            <tbody>
            @if(count($shortens)>0)
                @foreach($shortens as $shorten)
                    <tr>
                        <td>
                            <p>{{$shorten->created_at}}</p>
                        </td>

                        <td>
                            <a href="{{url($shorten->long)}}" >
                                <p class="text-warning">{{$shorten->long}}</p>
                            </a>
                        </td>
                        <td>
                            <input id="shorturl{{$shorten->id}}" class="form-control" type="text" value="http://www.short.local/t/{{$shorten->short}}" readonly>
                        </td>

                        <td>
                            <button onclick="copy(this)" id="copyBtn" value="{{$shorten->id}}" type="button" class="btn btn-info">copy</button>
                        </td>

                        <td>
                            <p>{{$shorten->view}}</p>
                        </td>
                    </tr>
                @endforeach
            @endif

            </tbody>
        </table>
    </div>






@endsection

<script>
    function copy(clickedBtn) { //clickedBtn คือ paramiter เฉยๆ
        var id = clickedBtn.value;
        var copyText = document.querySelector('#shorturl'+id);
        copyText.select();
        document.execCommand('copy');//function copy ของ javascript
        alert('Copied '+ copyText.value);
    }
</script>
