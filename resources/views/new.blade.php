@extends('layouts.app')
@section('content')
    <h1 class="mt-3" style="text-align: center">SHORTEN URL</h1>

    <form method="new" action="{{url('/')}}" style="text-align: center">
        @csrf
        <button type="submit" class="btn btn-outline-secondary" style="margin-top: 15px;">VIEW SHORT URL</button>
    </form>
{{--/////////////////////////////////////////////////////////////////--}}

    <div class="card text-center" style="margin-top: 20px;">
        <div class="card-header">
            <label>Long URL</label>

        <form method="post" action="{{url('/')}}">
            @csrf
            <div class="form-group">
                <input type="text" name="long" class="form-control">
            </div>
{{--            <button type="submit" ></button>--}}
            <button type="submit" class="btn btn-primary btn-info" style="margin-top: 5px;">CREATE SHORT URL</button>
        </form>

        </div>
    </div>
@endsection
